<?php

require_once SABRES_PLUGIN_DIR . '/library/fail.php';
require_once SABRES_PLUGIN_DIR.'/_inc/settings.php';

class Get_Settings {
    public function execute($rpc_data) {

      $fields = null;

      if ( !empty( $rpc_data['fields'] ) ) {
          $fields = explode( ',', $rpc_data['fields'] );
      }
      $settings=SbrSettings::instance();
      $res = $settings->get_json( $fields );
      echo $res;
    }

}
