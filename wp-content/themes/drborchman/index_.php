<?php get_header(); ?>
 	<div id="heroImage">
 		<img src="<?php echo catch_that_image() ?>" alt="<?php the_title(); ?>" />
 	</div>

 	<div id="main">
 		<div id="welcomeSection">
 			<?$id = 24;
			$post = get_post($id); 
			$content = $post->post_content;
			$title = $post->post_title;
			?>
 			<h1><?=$title?></h1>
 			<p><?=$content?></p>
 		</div>

		<div class="meetSection" id="meetTheDoctor">
	 		<div id="meetImage">
	 			<img src="<?php echo doctor_portrait_image() ?>" alt="<?php the_title(); ?>" />
	 		</div>
	 		<div id="meetText">
	 			<?$id = 20;
  				$post = get_post($id); 
  				$content = $post->post_content;
  				$title = $post->post_title;
  				?>
	 			<h1><?=$title?></h1>
				<p><?=$content?></p>
	 		</div>
		</div>

		<div id="policySection">
			<?$id = 27;
			$post = get_post($id); 
			$content = $post->post_content;
			$title = $post->post_title;
			?>
 			<h1><?=$title?></h1>
 			<p><?=$content?></p>
 			<!-- <p>Welcome to the office of Dr. Stephen D. Borchman. Dr. Borchman specializes in pediatric care from infancy through adolescence. He is a Diplomat of the American Board of Pediatrics. </p>

 			<p>The office may be closed alternating Wednesdays and Thursdays, and every other weekend. Almost always Dr. Patarino will be available in these circumstances to see your child.</p>

 			<p>Sick appointments are scheduled the same day as requested without exception. Although regular office hours end at 5:45 PM, emergency sick appointments will be made available until 8 PM. Please call as early in the day as possible to request this appointment, but should you get the answering service, simply instruct the service to have the doctor on call paged.</p>

 			<p>Well baby visits are scheduled for the first hour of each day and first hour after lunch in order to protect them from ill patients.</p>

 			<p><span class="uppercase">Please Note: Hours posted below are when the office is open and the secretaries are in to handle the calls.</span></p>

 			<p>Appointment times are different from posted below.</p>
 			<span class="indent">
	 			<p>• Well appointments must be made at least two weeks in advance.</p>
	 			<p>• Sick children will be seen the same day.</p>
	 			<p>• Please make arrangements for childcare givers to bring an ill child to the office, if at all possible, if both parents are working.</p>
 			</span>

 			<p>When Dr. Borchman is not available, your child can be seen by our covering physician, Dr. Charles Patterino, 718.494.6400 </p>

 			<p>Should you feel your child needs immediate medical services when my office is closed, it is always best to check with the answering service first, to see if either Dr. Borchman or Dr. Patarino is available to see the child. Often, just simple advice is all that is necessary, and the child can easily be seen in the office during regular hours the next day. Of course, if you feel your child has a life-threatening emergency, then by all means call 911 or proceed to the nearest emergency room.</p> -->
 		</div>

 		<div id="hoursSection">
 			<div id="hoursIcon">
 				<img src="<?php bloginfo('template_directory'); ?>/images/clock-icon.png">
 				<h1>Hours</h1>	
 			</div>

 			<div id="hoursSchedule">
 				<table>
				  <tr>
				    <td>Monday</td>
				    <td><img src="<?php bloginfo('template_directory'); ?>/images/arrow-icon.png"></td> 
				    <td class="timeSlot">9AM - 5:45PM</td>
				  </tr>

				  <tr>
				    <td>Tuesday</td>
				    <td><img src="<?php bloginfo('template_directory'); ?>/images/arrow-icon.png"></td> 
				    <td class="timeSlot">9AM - 5:45PM</td>
				  </tr>

				  <tr>
				    <td>Wednesday</td>
				    <td><img src="<?php bloginfo('template_directory'); ?>/images/arrow-icon.png"></td> 
				    <td class="timeSlot">9AM - 5:45PM</td>
				  </tr>

				  <tr>
				    <td>Thursday</td>
				    <td><img src="<?php bloginfo('template_directory'); ?>/images/arrow-icon.png"></td> 
				    <td class="timeSlot">9AM - 5:45PM</td>
				  </tr>

				  <tr>
				    <td>Friday</td>
				    <td><img src="<?php bloginfo('template_directory'); ?>/images/arrow-icon.png"></td> 
				    <td class="timeSlot">9AM - 5:45PM</td>
				  </tr>

				  <tr>
				    <td>Saturday</td>
				    <td><img src="<?php bloginfo('template_directory'); ?>/images/arrow-icon.png"></td> 
				    <td class="timeSlot">9:30AM - 2PM</td>
				  </tr>

				  <tr>
				    <td>Sunday</td>
				    <td><img src="<?php bloginfo('template_directory'); ?>/images/arrow-icon.png"></td> 
				    <td class="timeSlot">10AM - 2PM</td>
				  </tr>
				</table>
 			</div> <!-- hoursSchedule -->
 			</div> <!-- hoursSection -->

 			<div id="plansSection">
 				<h1>Insurance Plans</h1>
 				<div id="plansProviders">
 					<ul>
 						<li><b>Aetna</b> <br />Tricare</li>
 						<li><b>Affinity</b> <br />UMR</li>
 						<li><b>Amerigroup</b> <br />United Healthcare</li>
 						<li><b>Cigna</b> <br />United Healthcare Community Plan</li>
 						<li><b>Emblem Health</b> <br />1199</li>
 						<li><b>Empire B/C</b> <br />NO Hip Select</li>
 						<li><b>Empire Pathway</b> <br />NO Comprehealth</li>
 					</ul>	
 				</div>	
 			</div>

 			<div id="contactSection">

 				<h1>Contact Us</h1>
 				<div class="contactDivs">
	 				<div class="contactBlock">
	 					<?$id = 42;
						$post = get_post($id); 
						$content = $post->post_content;
						?>
	 					<img src="<?php bloginfo('template_directory'); ?>/images/pinpoint-icon.png">
	 					<h6><?=$content?></h6>
	 					<!-- <h6><?=$content?></h6>
	 					<h6><?=$content?></h6> -->
	 					<!-- <h6>54 Preston Avenue</h6>
	 					<h6>Staten Island</h6>
	 					<h6>NY 10312</h6>	 -->
	 				</div>
	 				<div class="contactBlock contactPhone">
	 					<?$id = 45;
						$post = get_post($id); 
						$content = $post->post_content;
						?>
	 					<img src="<?php bloginfo('template_directory'); ?>/images/phone-icon.png">
	 					<h6><?=$content?></h6>
	 					<!-- <h6>718-608-1347</h6> -->
	 				</div>
	 				<div class="contactBlock">
	 					<img src="<?php bloginfo('template_directory'); ?>/images/map-icon.png">
	 					<!-- <span class=""><?php echo do_shortcode("[gmw id=2]"); ?></span> -->
	 					<a class="gmw-thumbnail-map gmw-lightbox-enabled" href="#gmw-dialog-googlemapswidget-2" title="Click to open larger map"><h3>View Map</h3></a>
	 						
	 				</div>
<div class="contactBlock">
	<div id="fb">		
	<?php if(is_active_sidebar('sidebar-1')){
							dynamic_sidebar('sidebar-1');
							}
							?>
						</div>
				</div>
				</div>

 				<div class="contactChecker">
 					<div class="contactCheckerButton">
 						<a href="http://pediatricmed.alliedphysiciansgroup.com/patient-resources/symptom-checker/" target="blank"><h2>Symptom Checker</h2></a>	
 					</div>
 					<div class="contactCheckerButton">
 						<a href="http://pediatricmed.alliedphysiciansgroup.com/patient-resources/dosage-checker/" target="blank"><h2>Dosage Checker</h2></a>
 					</div>	
 				</div>


 				<div id="sponsorBox">
 					<img src="<?php bloginfo('template_directory'); ?>/images/allied-logo.png">
 					<h6>Allied Physicians Group</h6>
 					<button><a href="http://alliedphysiciansgroup.com/" target="blank">Visit</a></button>
 				</div>

 			</div>

<?php get_footer(); ?>
