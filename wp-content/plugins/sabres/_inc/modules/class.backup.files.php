<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}

/*
 * Copyright 2016 Sabres Security Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


if ( !class_exists( 'SBS_Backup_Files' ) ) {
    require_once SABRES_PLUGIN_DIR.'/library/fail.php';

    /**
     * The Sabres Backup Files Class
     *
     * @author Ariel Carmely - Sabres Security Team
     * @package Sabres_Security_Plugin
     * @since 1.0.0
     */
    final class SBS_Backup_Files
    {
        private $settings;
        private $full_path;
        private $output_file;

        public function __construct() {
        }

        public function register_event_callback( $event_name, $callback ) {
            if ( !isset( $this->_event_callbacks[$event_name] ) ) {
                $this->_event_callbacks[$event_name] = array();
            }

            array_push( $this->_event_callbacks[$event_name], $callback );
        }

        private function event_trigger( $event_name, $args ) {
            if ( !empty( $this->_event_callbacks[$event_name] ) ) {
                $callbacks = $this->_event_callbacks[$event_name];

                foreach ( $callbacks as $callback ) {
                    if ( !empty( $args ) ) {
                        call_user_func_array( $callback, $args );
                    } else {
                        call_user_func( $callback );
                    }
                }
            }
        }

        public function init( $settings = null, $full_path, $output_file ) {
            $this->settings = (object) array_change_key_case( $settings, CASE_LOWER );

            $this->full_path = $full_path;
            $this->output_file = $output_file;
        }

        public function is_valid() {
            $is_valid = true;
            $fail_args=array('code'=>500,
                            'includeBacktrace'=>true);

            // Check variables
            if ( empty( $this->full_path ) ) {
                $is_valid = false;
                $fail_args['message']='Full path can not be empty';
                SBS_Fail::byeArr($fail_args);
            }
            if ( empty( $this->output_file ) ) {
                $is_valid = false;
                $fail_args['message']='Output file can not be empty';
                SBS_Fail::byeArr($fail_args);
            }

            // Check for disk space
            $disk_space = disk_free_space( dirname( $this->output_file ) );
            $size_files = filesize( $this->full_path );
            if ( $size_files > $disk_space ) {
                $is_valid = false;
                $fail_args['message']='file size is greater then free disk space.';
                $fail_args['logData']=array('files_size'=>var_export($size_files,true),'free_space'=>var_export($disk_space,true));
            }

            $memory_limit = ini_get('memory_limit');
            if ( preg_match('/^(\d+)(.)$/', $memory_limit, $matches )) {
                if ( $matches[2] == 'M' ) {
                    $memory_limit = $matches[1] * 1024 * 1024; // nnnM -> nnn MB
                } else if ( $matches[2] == 'K' ) {
                    $memory_limit = $matches[1] * 1024; // nnnK -> nnn KB
                }
            }

            $totalSizeFolder = self::folderSize( ABSPATH );

            // TODO :: get '0.7' to setting
            if ( $totalSizeFolder * 0.7 >  $memory_limit ){
                $is_valid = false;
                $fail_args['message']='Total size folder is too close to memory limit';
                $fail_args['logData']=array('total_size_folder'=>var_export($totalSizeFolder,true),'memory_limit'=>var_export($memory_limit,true));
            }

            // Check execution time limit
            if ( !ini_set( 'max_execution_time', '0' ) && !set_time_limit( 0 ) ) {
                $is_valid = false;
                $fail_args['message']='Can not execute due too invalid time limits';
                $fail_args['logData']=array('max_execution_time'=>var_export(ini_set( 'max_execution_time', '0' ),true),
                'set_time_limit'=>var_export(set_time_limit( 0 ) ,true));
            }

            // Check execution memory limit
            if ( !ini_set( 'memory_limit', '-1' ) ) {
                $is_valid = false;
                $fail_args['message']='Can not execute due to invalid memory limit';
                $fail_args['logData']=array('memory_limit'=>var_export(ini_set( 'memory_limit', '-1' ),true));
            }

            return $is_valid;
        }

        static function folderSize ( $dir )
        {
            $size = 0;
            foreach ( glob( rtrim( $dir, '/' ) . '/*', GLOB_NOSORT ) as $each ) {
                $size += is_file( $each ) ? filesize( $each ) : self::folderSize( $each );
            }
            return $size;
        }

        public function run() {
            if ( $this->is_valid() ) {
                // Disable time limit
                ini_set( 'max_execution_time', '0' );
                set_time_limit( 0 );

                // Disable memory limit
                ini_set( 'memory_limit', '-1' );

                SBS_Zip::archive( ABSPATH, $this->output_file );
            }
        }
    }
}
