<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}

/*
 * Copyright 2016 Sabres Security Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


if ( !class_exists( 'SBS_Backup_DB_Engine' ) ) {
    require_once SABRES_PLUGIN_DIR.'/library/db.php';
    require_once SABRES_PLUGIN_DIR.'/library/fail.php';
    /**
     * The Sabres Backup Database Engine Class
     *
     * @author Ariel Carmely - Sabres Security Team
     * @package Sabres_Security_Plugin
     * @since 1.0.0
     */
    final class SBS_Backup_DB_Engine
    {
        private $settings;
        private $dump_file_name;

        public function __construct() { }

        public function register_event_callback( $event_name, $callback ) {
            if ( !isset( $this->_event_callbacks[$event_name] ) ) {
                $this->_event_callbacks[$event_name] = array();
            }

            array_push( $this->_event_callbacks[$event_name], $callback );
        }

        private function event_trigger( $event_name, $args ) {
            if ( !empty( $this->_event_callbacks[$event_name] ) ) {
                $callbacks = $this->_event_callbacks[$event_name];

                foreach ( $callbacks as $callback ) {
                    if ( !empty( $args ) ) {
                        call_user_func_array( $callback, $args );
                    } else {
                        call_user_func( $callback );
                    }
                }
            }
        }

        public function init( $settings = null, $dump_file_name ) {
            if ( !empty( $settings ) ) {
                $this->settings = (object) array_change_key_case( $settings, CASE_LOWER );
            }
            $this->dump_file_name = $dump_file_name;
        }

        public function is_valid() {
            $is_valid = true;
            $fail_args=array('code'=>500,
                            'includeBacktrace'=>true);


            // Check PDO
            if ( !extension_loaded('PDO') ) {
                $is_valid = false;
                $fail_args['message']='PDO extension is required';
                SBS_Fail::byeArr($fail_args);
            }

            // Check vendor
            if ( !class_exists( 'Ifsnop\Mysqldump\Mysqldump' ) ) {
                require_once SABRES_PLUGIN_DIR . '/library/vendor/autoload.php';
                if ( !class_exists( 'Ifsnop\Mysqldump\Mysqldump' ) ) {
                  $is_valid = false;
                  $fail_args['message']='Ifsnop\Mysqldump\Mysqldump Class does not exist';
                  SBS_Fail::byeArr($fail_args);
                }
            }


            // Check variables
            if ( empty( $this->dump_file_name ) ) {
                $is_valid = false;
                $fail_args['message']='dump_file_name can not be empty';
                SBS_Fail::byeArr($fail_args);
            }



            // Check is free disk
            $db_size = SBS_DB::get_db_total_size();
            $free_space = disk_free_space( ABSPATH );

            if ( $db_size >= $free_space ) {
                $is_valid = false;
                $fail_args['message']='DB size is greater then free disk space.';
                $fail_args['logData']=array('db_size'=>var_export($db_size,true),'free_space'=>var_export($free_space,true));
                SBS_Fail::byeArr($fail_args);
            }

            return $is_valid;
        }

        public function run() {
            if ($this->is_valid()) {
                // Disable time limit
                @ini_set( 'max_execution_time', '0' );
                @set_time_limit( 0 );

                // Disable memory limit
                @ini_set( 'memory_limit', '-1' );

                $dumpSettings = array(
                    'compress'=> \Ifsnop\Mysqldump\Mysqldump::GZIP
                );

                try {
                    $dump = new Ifsnop\Mysqldump\Mysqldump( 'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=' . DB_CHARSET, DB_USER, DB_PASSWORD, $dumpSettings );
                    $dump_dir = WP_CONTENT_DIR . '/sbs-backup';

                    if ( !is_dir( $dump_dir ) ) {
                        if ( !mkdir( $dump_dir ) ) {
                            $this->event_trigger( 'error', array(
                                'desc' => "No permissions to create folder: $dump_dir"
                            ) );

                            return;
                        }
                    }

                    $dump_file_name = $dump_dir . '/' . $this->dump_file_name .'.sql.gz';
                    $dump->start( $dump_file_name );

                } catch (\Exception $e) {
                    $this->event_trigger( 'error', array(
                        'desc' => $e->getMessage()
                    ) );
                    SBS_Fail::byeArr(array( 'message'=>"Exception has occured during backup. ".$e->getMessage(),
                                       'code'=>500,
                                       'includeBacktrace'=>false,
                                       'logData'=>$e->getTrace()
                                      ));
                }
            }
        }
    }
}
