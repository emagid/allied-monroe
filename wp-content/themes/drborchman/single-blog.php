

<?php get_header(); ?>

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<div id="primary" class="content-area">

		
		<main id="main" class="site-main site-blog" role="main">
		<div class="blogSection" id="<?php the_field('title'); ?>">
	 		
	 		<div id="blogText">
	 			<h1><?php the_field('title'); ?></h1>

	 			<h6><?php echo get_the_date( get_option('date_format') ); ?></h6>
	 			<img src="<?php the_field('image'); ?>">
				<p><?php the_field('post'); ?></p>
	
				<div class="share-buttons"><?php echo do_shortcode("[ssba]"); ?></div>
				<div class="back-link">
					<a href="/blog">< Back to Blog</a>
				</div>
	 		</div>
		</div>


		</main><!-- .site-main -->
	</div><!-- .content-area -->


		<?php endwhile; endif; ?>


<?php get_footer(); ?>


<style type="text/css">
.blogSection {
	margin-left: 0;
	height: 100%;
	overflow: auto;
}
#blogText {
	width: 70%;
	height: 100%;
	background-size: cover;
	text-align: center;
}
#blogText h1 {
	text-align: center;
	padding-left: 0;
}
#blogText h6 {
	text-align: center;
	padding-left: 0;
	width: 100%;
}
#blogText img {
	max-width: 50%;
	text-align: center;
	margin: 15px auto;
}
#blogText p {
	height: 100%;
	overflow: auto;
	width: 80%;
    margin: 0 auto;
    padding-left: 0;
    text-align: left;
    font-size: 18px;
}
#blogText a {
	color:#fff;
	
}
.back-link {
	text-align: right;
	margin: 20px auto;
	width: 90%;
}
.share-buttons {
	width: 90%;
	margin: 10px auto;
}
@media (max-width: 840px) {
	#blogText {
		width: 100%;
	}
}
</style>
