<?php

require_once SABRES_PLUGIN_DIR . '/library/fail.php';
require_once SABRES_PLUGIN_DIR . '/_inc/class.logger.php';

class Clear_Log {
    public function execute($rpc_data) {

      $start = null;
      $end = null;

      if ( !empty( $rpc_data['start'] ) && !empty( $rpc_data['end'] ) ) {
              $start = $rpc_data['start'];
              $end = $rpc_data['end'];
      }
      $logger= SBS_Logger::getInstance();
      $res = json_encode( $logger->clear_entries( $start, $end ) );
      echo $res;

    }

}
